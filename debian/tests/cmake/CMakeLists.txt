cmake_minimum_required(VERSION 3.15)

project(find_juce CXX C)

find_package(JUCE CONFIG REQUIRED)

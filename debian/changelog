juce (8.0.6+ds-2) unstable; urgency=medium

  * Disable NEON support if the compiler does not support it
  * autopkgtest: Handle NEON support in upstream unittests

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Mon, 10 Feb 2025 11:36:11 +0100

juce (8.0.6+ds-1) unstable; urgency=medium

  [ IOhannes m zmölnig (Debian/GNU) ]
  * New upstream version 8.0.6+ds
    + Refresh patches
  * Update copyright information
    + Re-generate d/copyright_hints
    + Bump copyright dates
  * Apply 'wrap-and-sort -ast'

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Mon, 27 Jan 2025 13:44:11 +0100

juce (8.0.4+ds-1) unstable; urgency=medium

  [ IOhannes m zmölnig (Debian/GNU) ]
  * New upstream version 8.0.4+ds
    + Refresh patches
  * Fix inclusion of juce_vst3_helper in cmake snippets
  * Only attempt to set "-mabi=ieeelongdouble" on ppc64el (not on ppc64/powerpc)
  * autopkgtest: declare that we (also) need a C-compiler
  * Update copyright information
    + Update d/copyright
    + Re-generate d/copyright

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Tue, 19 Nov 2024 16:36:54 +0100

juce (8.0.3+ds-2) unstable; urgency=medium

  * Fixes for non-mainstream archs
    + On PowerPC, build with "-mabi=ieeelongdouble"
    + Use llround() rather than naive casts for converting time to ms
  * Fix autopkgtests for non-mainstream archs
    + Skip unittests on i386 that cannot succeed
    + Skip autopktest on s390x
    + Ensure that ~/.config is present for autopkgtests

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Tue, 29 Oct 2024 22:54:14 +0100

juce (8.0.3+ds-1) unstable; urgency=medium

  [ IOhannes m zmölnig (Debian/GNU) ]
  * New upstream version 8.0.3+ds
    + Drop obsolete patch
    + Refresh patches
  * Provide stub HOME in autopkgtest (if there is none)
  * Apply 'wrap-and-sort -ast'
  * Update copyright information
    + Re-generate d/copyright_hints
    + Update d/copyright

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Sat, 19 Oct 2024 18:09:01 +0200

juce (8.0.0+ds-1) unstable; urgency=medium

  * New upstream version 8.0.0+ds
    + Refresh patches
  * Add patch to allow building on 32bit platforms
  * Exclude AAX-Framework
  * (Build-)Depend on libfontconfig-dev
  * B-D on pkgconf rather than pkg-config (using the latter as a fallback)
  * Update d/README.source
    + Drop 'gbp clone' info
    + Note that we do not de-vendor libharfbuzz
  * Update copyright information
    + Upstream switched codebase from ISC to AGPL-3
    + Re-generate d/copyright

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Mon, 17 Jun 2024 14:37:57 +0200

juce (7.0.12+ds-1) unstable; urgency=medium

  * Upload to unstable.

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Wed, 05 Jun 2024 17:33:44 +0200

juce (7.0.12+ds-1~exp1) experimental; urgency=medium

  * New upstream version 7.0.12+ds
    + Refresh patches
  * Bump standards version to 4.7.0

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Sun, 21 Apr 2024 21:36:59 +0200

juce (7.0.11+ds-1~exp2) experimental; urgency=medium

  * Team upload
  * Add patch to switch from webkit2gtk 4.0 to 4.1
  * Update juce-modules-source dependency (Closes: #1068484)

 -- Jeremy Bícha <jbicha@ubuntu.com>  Thu, 11 Apr 2024 10:14:55 -0400

juce (7.0.11+ds-1~exp1) experimental; urgency=medium

  * New upstream version 7.0.11+ds
  * Update copyright information
    + Bump copyright dates
    + Re-generate d/copyright_hints

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Mon, 08 Apr 2024 09:42:49 +0200

juce (7.0.9+ds-1~exp1) experimental; urgency=medium

  * New upstream version 7.0.9+ds
    + Refresh patches
  * Update copyright information
    + Re-generate d/copyright_hints

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Mon, 11 Dec 2023 18:27:30 +0100

juce (7.0.8+ds-1~exp1) experimental; urgency=medium

  [ IOhannes m zmölnig (Debian/GNU) ]
  * New upstream version 7.0.8+ds
    + Refresh patches
  * Adjust to upstream filenames (BREAKING_CHANGES.md, CHANGE_LIST.md)
  * Update copyright information
    + Re-generate d/copyright_hints

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Fri, 10 Nov 2023 12:01:42 +0100

juce (7.0.7+ds-1~exp1) experimental; urgency=medium

  * New upstream version 7.0.7+ds
    + Refresh patches
  * Build juce_vst3_helper as well
  * Fix 'nodoc' build-profile
    + Do not build libjuce-doc in the 'nodoc' profile
  * Fix arch:all builds
  * Clean apiref-directory (Closes: #1045638)
  * Update copyright information
    + Bump copyright dates
    + Re-generate d/copyright_hints

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Thu, 14 Sep 2023 16:47:42 +0200

juce (7.0.5+ds-2) unstable; urgency=medium

  * Team upload
  * Add patch to switch from webkit2gtk 4.0 to 4.1
  * Update juce-modules-source dependency (Closes: #1068484)

 -- Jeremy Bícha <jbicha@ubuntu.com>  Thu, 11 Apr 2024 10:47:48 -0400

juce (7.0.5+ds-1) unstable; urgency=medium

  * New upstream version 7.0.5+ds
    + Refresh patches
  * Replace obsolete Dependencies with their modern counterparts

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Sat, 28 Jan 2023 21:00:41 +0100

juce (7.0.4+ds-2) unstable; urgency=medium

  * Prevent building on 'armel' altogether

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Thu, 12 Jan 2023 09:20:17 +0100

juce (7.0.4+ds-1) unstable; urgency=medium

  * New upstream version 7.0.4+ds
    + Drop patches applied upstream
  * Switch repack-suffix to '+ds'
  * Update dates in d/copyright
    + Re-generate d/copyright_hints
    + Ensure that 'licensecheck' is run with the C.UTF-8 locale
    + Drop duplicate 'licensecheck' target
  * Bump standards version to 4.6.2

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Wed, 11 Jan 2023 16:38:55 +0100

juce (7.0.3~ds0-1) unstable; urgency=medium

  * New upstream version 7.0.3~ds0
    + Refresh patches
  * Modernize 'licensecheck' target
    + Re-generate d/copyright_hints

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Wed, 30 Nov 2022 14:02:44 +0100

juce (7.0.2~ds0-3) unstable; urgency=medium

  * Fix typos in patch descriptions
  * Patch to ensure that juce_core links against zlib (Closes: #1022281)
  * Add patch to make documentation reproducible

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Thu, 10 Nov 2022 13:55:28 +0100

juce (7.0.2~ds0-2) unstable; urgency=medium

  * d/watch: Switch to github/tags
  * Explicitly link Projucer/juceaide against zlib

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Wed, 28 Sep 2022 11:41:06 +0200

juce (7.0.2~ds0-1) unstable; urgency=medium

  * New upstream version 7.0.2~ds0
    * Refresh patches
  * Drop debian_vst patch and accompanying legacy header
    (Rely on fst-dev instead)
    (Closes: #1017554)
  * Have 'juce-modules-source' Recommend 'fst-dev'
  * Fix path to juce-icon
  * Don't build arch:armel packages
  * Re-generate d/copyright_hints
  * Run 'wrap-and-sort -ast'

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Fri, 19 Aug 2022 20:00:36 +0200

juce (7.0.1~ds0-1) unstable; urgency=medium

  * New upstream version 7.0.1~ds0
  * Move juceaide/juce_lv2_helper to juce-modules-source
    + Install them into a private location
    + Declare versioned Breaks/Replaces against juce-tools
  * Fix path to juce_lv2_helper (Closes: #1014575)
  * Add autopkgtest for CMake snippets
    Thanks to Andreas Beckmann <anbe@debian.org>
  * Install apiref via dh_installdocs
  * Run dh_doxygen after dh_installdocs

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Thu, 04 Aug 2022 22:02:56 +0200

juce (7.0.0~ds0-1) unstable; urgency=medium

  * New upstream version 7.0.0~ds0
    (Closes: #1012954)
    + Refresh patches
    + Drop patches applied upstream
  * Upstream now natively supports LV2
    + Drop additional LV2-files, now that JUCE supports LV2 natively
    + Build and install the juce_lv2_helper
  * Don't try to install non-existant paths
  * Update d/copyright
    + Drop unused AGPL text
    + Regenerate d/copyright_hints
  * Bump standards version to 4.6.1

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Tue, 28 Jun 2022 08:25:52 +0200

juce (6.1.6~ds0-1) unstable; urgency=medium

  * New upstream version 6.1.6~ds0
    + Refresh patches
  * Regenerate d/copyright_hints

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Fri, 18 Mar 2022 18:31:11 +0100

juce (6.1.5~ds0-1) unstable; urgency=medium
  * New upstream version 6.1.5~ds0
    + Refresh patches
  * Update dates in d/copyright
    + Regenerate d/copyright_hints

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Mon, 31 Jan 2022 11:55:24 +0100

juce (6.1.4~ds0-1) unstable; urgency=medium

  * New upstream version 6.1.4~ds0
    - Refresh patches
  * Regenerated d/copyright_hints

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Mon, 10 Jan 2022 09:12:48 +0100

juce (6.1.3~ds0-1) unstable; urgency=medium

  * Upload to unstable.

  * Remove JUCE from arch:armel
    - Drop hack to make JUCE compile on armel

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Thu, 23 Dec 2021 10:05:10 +0100

juce (6.1.3~ds0-1~exp3) experimental; urgency=medium

  * Fix typo in fix for MIPS build failure
  * Drop some unnecessary Build-Dependencies

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Wed, 22 Dec 2021 15:11:56 +0100

juce (6.1.3~ds0-1~exp2) experimental; urgency=medium

  * Fix FTBFS on MIPS architectures
  * Fix FTBFS on armel
  * Fix quoting when defining DEBIAN_JUCEPROJECT_LIBS
  * Mark 'juce-tools' as Multi-Arch foreign

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Wed, 22 Dec 2021 12:02:02 +0100

juce (6.1.3~ds0-1~exp1) experimental; urgency=medium

  * New upstream version 6.1.3~ds0

  * Refresh patches
  * Use CMake to build
  * Fix paths in installed cmake snippets
  * Install ChangeList.txt to keep Projucer happy
  * Update LV2-wrapper
  * Move B-D on python3:any from B-D-Indep to B-D
  * Use DEB_*_MAIN_APPEND rather than passing build flags manually
  * Properly call dh_doxygen
  * Provide a static manpage
  * Make documentation reproducible
  * Apply 'wrap-and-sort -ast'
  * Drop no longer used variables TARGET_ARCH and DEB_SRCDIR

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Thu, 16 Dec 2021 13:12:40 +0100

juce (6.1.0~ds0-1~exp1) experimental; urgency=medium

  * New upstream version 6.1.0~ds0
    * Refresh patches
  * Update d/copyright
    * Regenerate d/copyright_hints
  * debian_vst:
    * Drop compat-defines
    * Add typedef for Vst2::AudioMasterOpcodesX
  * Bump standards version to 4.6.0

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Mon, 30 Aug 2021 14:58:56 +0200

juce (6.0.7~ds0-1~exp1) experimental; urgency=medium

  * New upstream version 6.0.7~ds0
    * Refresh patches
  * Fix spelling error in patch
  * d/watch: use repacksuffix rather than oversionmangle

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Tue, 26 Jan 2021 23:41:40 +0100

juce (6.0.5~ds0-1~exp1) experimental; urgency=medium

  * New upstream version 6.0.5~ds0 (Closes: #977307)
    * Drop useless patches
    * Refresh patches
    * Patch to allow building of build-helpers
  * Set CPPFLAGS to build GPL-variant of Projucer
  * Split juce-modules-source into an arch:any and an arch:all package.
    Thanks to Helmut Grohne <helmut@subdivi.de> (Closes: #979169)
  * Install CMake snippets in 'juce-modules-source'
  * B-D on cmake
  * Use "make install" to collect files for the packages
  * Update d/copyright
    * d/copyright: update "Source" field to new github project
    * Exclude .pc/ from licensecheck
    * Regenerate d/copyright_hints
  * Update d/watch to version 4
    * Update d/watch to new upstream github project
  * Refresh patches with 'gbp pq'
  * Bump dh-compat to 13
  * Bump standards version to 4.5.1

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Wed, 13 Jan 2021 14:12:19 +0100

juce (5.4.7~ds0-2) unstable; urgency=medium

  * Refresh patches using 'gbp pq'
  * Add patch to not fail fatally without X-server
  * Fix FTCBFS:
    + Let dpkg's buildtools.mk supply pkg-config for debian/rules.
    + cross.patch: Make pkg-config substitutable.
    + Let dh_auto_build pass cross tools to make.
    Thanks to Helmut Grohne <helmut@subdivi.de> (Closes: #951684)
  * Add patch to allow overriding of pkg-config in generated Makefiles

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Mon, 16 Mar 2020 13:35:45 +0100

juce (5.4.7~ds0-1) unstable; urgency=medium

  * New upstream version 5.4.7~ds0
  * Refresh d/copyright_hints

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Tue, 18 Feb 2020 09:46:05 +0100

juce (5.4.6~ds0-1) unstable; urgency=medium

  [ IOhannes m zmölnig ]
  * New upstream version 5.4.6~ds0
    * Drop patches applied upstream
    * Refresh patches
  * Fixed mimetype in Projucer.desktop (Closes: #944381)
  * Pass CPPFLAGS/CFLAGS/LDFLAGS when building lv2_ttl_generator
  * Remove single-quotes from mimecap entry
  * Added configuration for salsa-ci
  * Bump dh-compat to 12
  * Bump standards version to 4.5.0

  [ Olivier Humbert ]
  * Update copyright (http -> https)
  * Update libjuce-doc.doc-base (remove 1 empty line)
  * Update Projucer.desktop (add FR comment & GenericName)
  * Update README.libjuce-doc (remove 1 empty line)
  * Update copyright (add myself)

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Thu, 06 Feb 2020 10:34:24 +0100

juce (5.4.5~ds0-1) unstable; urgency=medium

  * New upstream version 5.4.5~ds0

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1

  [ IOhannes m zmölnig (Debian/GNU) ]
  * Update patches
  * Exclude all .git* files and directories from repackaged sources
  * Update d/watch
  * Regenerate d/copyright_hints

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Wed, 11 Dec 2019 15:12:06 +0100

juce (5.4.4~repack0-3) unstable; urgency=medium

  * Make Makefiles created by Debian's Projucer compile without further ado
    * Add patch to link against (required) system-libraries
    * Add patch to inject LIBS into Projucer's Makefile-exporter from the
      from the cmdline
    * Make armel/mipsel/... Projucer add "-latomic" to LIBS
  * Document the Projucer fixes in README.Debian

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Thu, 22 Aug 2019 10:01:56 +0200

juce (5.4.4~repack0-2) unstable; urgency=medium

  * Link against "libatomic" on armel/mipsel/... to fix FTBFS
  * Fix dversionmangle in d/watch

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Wed, 21 Aug 2019 13:56:30 +0200

juce (5.4.4~repack0-1) unstable; urgency=medium

  * New upstream version 5.4.4~repack0
    (Closes: #925723)

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat
  * Bump Standards-Version to 4.4.0

  [ IOhannes m zmölnig ]
  * Refresh patches
  * Drop obsolete d/source/local-options
  * Update d/copyright
    * Regenerate d/copyright_hints

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Tue, 20 Aug 2019 13:28:00 +0200

juce (5.4.1+really5.4.1~repack-3) unstable; urgency=medium

  * Add more missing VST opcodes (Closes: #923529)

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Wed, 06 Mar 2019 19:22:02 +0100

juce (5.4.1+really5.4.1~repack-2) unstable; urgency=medium

  * Install VSTInterface.h for jor arch:all builds

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Sun, 13 Jan 2019 09:42:22 +0100

juce (5.4.1+really5.4.1~repack-1) unstable; urgency=medium

  [ IOhannes m zmölnig (Debian/GNU) ]
  * New upstream version 5.4.1
    * Refresh patches

  * Ported juce_VSTInterface.h from JUCE-5.3.2 to JUCE-5.4.1 (Closes: #913915)
  * Fixed spelling errors
  * Updated d/copyright
    * Fixed 'licensecheck' target
    * Refreshed d/copyright_hints
  * Removed trailing whitespace in d/changelog
  * Bumped standards-version to 4.3.0

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Sat, 12 Jan 2019 00:45:16 +0100

juce (5.4.1+really5.3.2~repack-1) unstable; urgency=medium

  * Reverted to upstream version 5.3.2 (due to VST issues)
    (Closes: #913915)

  * Fixed cmdline flag for Projucer for the autopkgtest
  * Updated licensecheck ignores

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Wed, 05 Dec 2018 17:06:00 +0100

juce (5.4.1~repack-1) unstable; urgency=medium

  * New upstream version 5.4.1~repack

  [ Olivier Humbert ]
  * Fixed typos in README.Debian and README.source

  [ IOhannes m zmölnig (Debian/GNU) ]
  * Refreshed patches and removed patch applied upstream
  * Updated d/copyright
    * Regenerated d/copyright_hints

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Wed, 14 Nov 2018 11:22:12 +0100

juce (5.3.2~repack-2) unstable; urgency=medium

  * Backported fix for Projucer crash when saving global search-path
  * Fixed patch-headers in preparation of 'gbp pq'
  * Declare that building this package doesn't require "root" privileges
  * Bumped standards-version to 4.2.1

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Fri, 26 Oct 2018 22:17:07 +0200

juce (5.3.2~repack-1) unstable; urgency=medium

  * New upstream version 5.3.2~repack
    * Refreshed patches
    * Dropped build-time patch for reproducible builds

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Mon, 14 May 2018 09:58:20 +0200

juce (5.3.1~repack-1) unstable; urgency=medium

  * New upstream version 5.3.1~repack
    * Refreshed patches
  * Patch to generate useful JACK client names by default
  * Run unit-tests on Deb-CI
    * Fix UnitTests to use system-wide JUCE modules
  * Refreshed d/copyright_hints
  * Bumped standards to 4.1.4

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Thu, 12 Apr 2018 13:34:24 +0200

juce (5.3.0~repack-1) unstable; urgency=medium

  * New upstream version 5.3.0~repack
    * Refreshed patches
    * Fixed path to Icon.png
  * Added patch to not reveal usage data by default
  * Install API documentation into /usr/share/doc/juce-modules-source/
  * Updated copyright for moved Box2D tests
  * Updated d/copyright_hints
  * Updated Vcs-* stanzas to salsa.d.o
  * Updated upstream source URL

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Mon, 26 Mar 2018 16:11:09 +0200

juce (5.2.1~repack-2) unstable; urgency=medium

  * Fixed building of documentation (for real)
    * Use upstream's Makefile rather than second-guessing how to do it
    * Added B-D on Python3 and added patch to use Py3 to build docs
    * Added B-D on graphviz (Closes: #890035)
    * Dropped .tag files for (more) reproducible builds. (Closes: #890036)
      Thanks to Chris Lamb <lamby@debian.org> for the last two
  * Re-enabled LV2 support (Closes: #889969)
    * Refreshed LV2 sources

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Sun, 11 Feb 2018 10:42:03 +0100

juce (5.2.1~repack-1) unstable; urgency=medium

  * New upstream version 5.2.1~repack
    * Removed patches applied upstream
    * Refreshed remaining patches
  * Have juce-module-source depend on more packages
  * Fixed libjuce-doc package to actually contain documentation
  * Added more documentation on how to create Debian packages depending on JUCE
  * Updated maintainer email
  * Updated d/copyright(_hints)
  * Switched build system from CDBS to DH
  * Bumped dh-compat to 11
  * Bumped standards version to 4.1.3

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Thu, 08 Feb 2018 22:25:21 +0100

juce (5.2.0~repack-3) unstable; urgency=medium

  * Backported upstream fix for IIRFilter with SIMD registers
  * Updated README.Debian with build instructions for Debian Developers
  * Bumped standards version to 4.1.2

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Tue, 19 Dec 2017 17:51:50 +0100

juce (5.2.0~repack-2) unstable; urgency=medium

  * Projucer: use Debian's JUCE modules by default
  * modules-sources: force system provided use of libflac and libvorbis
    (LP: #1720634)
  * Recommend image/audio format libraries and depend on zlib-dev
  * Fixed formatting of NEWS file

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Tue, 19 Dec 2017 11:43:22 +0100

juce (5.2.0~repack-1) unstable; urgency=medium

  * New upstream version 5.2.0~repack

  * Refreshed patches
    - Dropped those applied upstream
    - Dropped unneeded patch magic
    - Removed reproducible DATE_TIME hacks
    - Patch to fix spelling errors
    - Patch to avoid checking for updates
    - Added DEP-3 headers to patches
    - Build GPL-3 variant of Projucer
  * Added missing B-Ds, dropped unused B-Ds
  * Dropped libjuce(0|-dev) packages
    - juce-modules-source breaks/replaces libjuce-dev
  * Re-ordered packages in d/control
  * Installed lv2-ttl-generator as part of juce-tools
  * "verbose" builds
  * Dropped 'install' target
  * Link with "--as-needed"
  * Changed how to build debian-specific artifacts
  * Force-disabled embedded zlib/jpeglib/pnglib
  * Added "Keywords" to desktop-file
  * Ship BREAKING-CHANGES.txt
  * Updated README.Debian
  * Added d/NEWS
  * Updated d/copyright
    - Excluded some more well-known binary files from licensecheck
  * Modernized Vcs-Browser stanza
  * Fixed reference to non-existing "introjucer" package in long description
  * Dropped unused lintian override
  * Bumped standards version to 4.1.1

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Thu, 09 Nov 2017 15:04:02 +0100

juce (4.3.0~repack-1) unstable; urgency=medium

  * New upstream version 4.3.0~repack

  [ IOhannes m zmölnig ]
  * Refreshed patches.
  * Fixed LV2_wrapper.
    * Recommend lv2-dev (for building LV2-plugins)
  * Tightened versioned dependencies.
  * Added Multi-Arch fields.
  * Updated d/copyright(_hints)

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Fri, 04 Nov 2016 22:27:09 +0100

juce (4.2.4~repack-1) unstable; urgency=medium

  * New upstream version 4.2.4~repack
  * Refreshed patches.
  * Enabled VST pluginhost
  * Tightened dependency of juce-utils
  * Update d/copyright_hints
  * Excluded more binary data from copyright-check

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Wed, 21 Sep 2016 12:21:37 +0200

juce (4.2.3~repack-1) unstable; urgency=medium

  * New upstream version 4.2.3~repack

  * Refreshed patches
  * Documented how to properly checkout the project via gbp
  * Updated d/copyright_hints

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Mon, 08 Aug 2016 15:47:12 +0200

juce (4.2.2+repack-1) unstable; urgency=medium

  * Imported Upstream version 4.2.2+repack

  * Fixed rule that fixes permissions of source-files
  * Renamed introjucer/projucer package to juce-tools
    * Updated README.Debian to reflect package rename
    * Shortened extended description
  * Updated d/copyright_hints

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Wed, 25 May 2016 17:01:46 +0200

juce (4.2.1+repack-2) unstable; urgency=medium

  * Drop "-march=native" from Projucer build-flags
  * Refreshed libpng16 patch

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Mon, 02 May 2016 15:51:34 +0200

juce (4.2.1+repack-1) unstable; urgency=medium

  * Imported Upstream version 4.2.1+repack
    * Introjucer has been replaced by Projucer
  * Refreshed patches
  * libjuce: manually include AppConfig.h
  * libjuce0 symbols file (not used in packaging)
  * Updated d/copyright
    * Drop licenses that no longer apply from d/copyright
  * Install documentation into /usr/share/doc/libjuce/
  * Bumped standards version to 3.9.8

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Thu, 28 Apr 2016 23:38:46 +0200

juce (4.1.0+repack-6) unstable; urgency=medium

  * Updated d/copyright
  * Fixed homepage field.
    Thanks to Paul Wise <pabs@debian.org> (Closes: 820653)
  * Set BUILD_TIME from d/changelog

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Wed, 13 Apr 2016 16:39:10 +0200

juce (4.1.0+repack-5) unstable; urgency=medium

  [ IOhannes m zmölnig ]
  * LV2 support
    * Added LV2-wrapper sources to -dev package.
    * LV2-wrapper not included in library
    * ttl-generator for lv2
  * Another try on a decent build-date.
  * Clamped audio-plugin defines.

  [ Gianfranco Costamagna ]
  * Fixed build with libpng16. (Closes: 820347)

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Thu, 07 Apr 2016 22:18:20 +0200

juce (4.1.0+repack-4) unstable; urgency=medium

  * Updated B-D from libpng12-dev to libpng-dev.
    Thanks to Tobias Frost <tobi@debian.org> (Closes: #819438)

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Mon, 28 Mar 2016 21:17:16 +0200

juce (4.1.0+repack-3) unstable; urgency=medium

  * Dropped "-std=gnu++0x" from CFLAGS since we are already using "-std=c++11"
    (Closes: #816000)
  * Reverted uscan-compat to 3

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Sat, 27 Feb 2016 17:02:27 +0100

juce (4.1.0+repack-2) unstable; urgency=medium

  * Use numeric build-date
  * Nicer package description for 'introjucer'
  * Fixed debian/watch

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Tue, 16 Feb 2016 16:45:04 +0100

juce (4.1.0+repack-1) unstable; urgency=medium

  * Initial release. (Closes: #808611)

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Fri, 12 Feb 2016 07:46:59 +0100

JUCE for Debian
===============

upstream's preferred form of usage of JUCE is to include a verbatim copy of all
used JUCE modules in your application.
This is made explicit in the 'Projucer', JUCE's own software project
management workbench, that will copy (or symlink, or include otherwise) the
modules' source code into your project.

# Projucer for Debian

Installing the following packages will give you the 'Projucer' as Debian
packages while keeping your embedded-module-code workflow:
 - juce-tools (contains the Projucer)
 - juce-modules-source (contains the source-code for the JUCE modules)

The 'Projucer' as shipped with Debian has the following modification regarding
the once shipped by upstream:

# Debian packages that depend on JUCE

This is a quick guideline for packaging upstream software that uses JUCE for
Debian.
For further implementation details check out the 'iem-plugin-suite' package.

- Build-Depend on 'juce-modules-source'
- Add 'Built-Using: juce-modules-source (= <<version>>)' to debian/control
  (replace '<<version>>' with the actual version of 'juce-modules-source', as
  obtained with

       dpkg-query --show --showformat='${source:Version}' juce-modules-source


  E.g. dh based packages might use something like the following in debian/rules:

       JUCE_VERSION := $(shell dpkg-query --show --showformat='$${source:Version}' juce-modules-source)
       override_dh_gencontrol:
            dh_gencontrol -- -Vjuce:BuiltUsing="juce ( = $(JUCE_VERSION) )"

  Accompanied by the following in the binary package's section in debian/control:

       Built-Using: ${juce:BuiltUsing}

- If needed, dynamically link against the following libraries (as
  "juce-modules-source" does not include their sources (as opposed to upstream):
  - libjpeg
  - libpng
  - libflac
  - libvorbis libvorbisenc libvorbisfile
  - libogg
  - zlib
  E.g. using something like:

       make LDFLAGS="$(pkg-config --libs libpng libjpeg flac vorbis vorbisfile vorbisenc ogg zlib)"

  *Alternatively*, resave the JUCE-project with the Debian-packaged 'Projucer'
  (>=5.4.4~repack0-3) which will take care of adding these libraries (if
  required) to the LinuxMakefile build.

- When compiling for some embedded architectures (notably 'armel', 'mipsel' and
  the like), you might need to link against '-latomic'.
  The following snippet for d/rules can help inject the library on the required
  architectures:

        # link with libatomic on architectures without built-in atomic
        noatomicarch = $(shell dpkg-architecture -qDEB_HOST_ARCH | egrep -x "(armel|powerpc|powerpcspe|m68k|mips|mipsel|sh4|riscv64)")
        ifeq ($(if $(noatomicarch),atomic), atomic)
                LDFLAGS += -latomic
        endif

  *Alternatively*, resave the JUCE-project with the Debian-packaged 'Projucer'
  (>=5.4.4~repack0-3) which will take care of adding the relevant flags to the
  LinuxMakefile build.

- Also be aware, that Projucer generated makefiles usually include
  "-march=native", which is mostly not what you want for a generic baseline CPU
  build.
  You can override that by setting the TARGET_ARCH make variable

       make TARGET_ARCH=""
